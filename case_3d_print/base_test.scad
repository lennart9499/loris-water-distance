module base()
{
    difference() {
        union() {
            cube([60,31,17]);
            translate([-2.5,-2.5,0])cube([65,36,2]);
        }
        translate([2,2,2]) cube([56,27,17]);
        translate([2,10.5,-1]) cube([10,10,11]);
    }
}

module based()
{
    difference() {
        union() {
            cylinder(15, 23, 25);
            translate([0, 0, 15]) cylinder(32, 25, 25);
            translate([0, 0, 47]) cylinder(2, 28, 28);
        }
        translate([0, 0, -1]) cylinder(48, 23, 23);
        translate([0, 0, -1]) cylinder(52, 5, 5);
    }
}

echo(version=version());

based();
translate([-7, -15.5, 49]) base();
