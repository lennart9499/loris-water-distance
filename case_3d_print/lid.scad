module lid()
{
    difference() {
        cube([65,36,50]);
        translate([2,2,2]) cube([61,32,50]);
    }
}

echo(version=version());
lid();