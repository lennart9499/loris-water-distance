module base()
{
    difference() {
        union() {
            cube([60,31,17]);
            translate([-2.5,-2.5,0])cube([65,36,2]);
        }
        translate([2,2,2]) cube([56,27,17]);
        translate([2,10.5,-1]) cube([10,10,11]);
    }
}

echo(version=version());
base();
