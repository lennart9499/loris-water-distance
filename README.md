# LoRIS-Water-Distance

<sub><sup>Quick Disclaimer: All parts of this project are just very basic prototypes.
In many parts of this project are not yet fully developed and there is still much room for improvement.</sup></sub>

One instance of this project is hosted at [https://loris.lkai.de](https://loris.lkai.de).

### Description

This project was originally created as part of the LoRIS Innovation Cup 2022. I became aware of the competition through an email. As part of this competition, members of northern German colleges and universities can develop IoT projects based on LoRaWAN.

Since the competition was cancelled due to a low number of participants, it was possible to take part in the normal ELV readers' competition. The corresponding article in the ELV magazine is expected to be published in the edition 1/2023.

The basic idea of this project is to measure water levels of small lakes and brooks in order to have a bigger databasis for predictions of extreme weather events. But it also could be used in local personal environments like a small pond in the garden etc.

## Hardware

### 3D printed Case
For this project a very basic housing was designed in [OpenSCAD](https://openscad.org/). It can be found in the folder `case_3d_print`. For the slicing I've used Cura and for printing a Creality3D Ender 3v2 with blue PLA filament.

### Mounting
For the mounting I've used a 1m long HT pipe with an inner diameter of 50mm.
Then I drilled a small hole in the middle of an HT pipe end cap and glued the 3D printed housing on top.

### Electronic Components
As the microcontroller the LoRIS system by ELV Elektronik was used(Ref: [LoRIS Shop](https://de.elv.com/technik-fuer-elektronik-projekte/bausaetze/elv-modulsystem/))

The stack was build together with the battery module at the bottom followed by the base module and the distance sensor module.
For the software configuration the basic firmware from ELV was used by using the LoRIS Flash Tool from their website.
After that the instructions were followed to create a project in TTN 


## Software

### Installation
Requirements: `Python 3.8` installation(or newer) and `pip` installation

Install the requirements for both the backend and the webserver using `pip`:
`$ pip install -r requirements.txt`

Change the credentials to the TTN((The Things Network)[https://www.thethingsnetwork.org/]) MQTT access data in the file `credentials.json`.

### Usage
The main script for the backend is `backend/backend.py`, it is used to get the data from the TTN MQTT server and saves it in a local sqlite database.
This data is then visualized by running `webserver/run.py`, it creates a default flask server on port 5000.
Beware, that ports should not be opened on a server that is openly available, a proper proxy server like `nginx` and software like `fail2ban` should be used. 
The default user is `lorisadmin` with the password `LoR!S2022`.
It can be changed in the file `webserver/web_configuration.json`.
On the webfrontend different settings can be changed and stats viewed.

## Future Work
As mentioned above there are many points which need improvement, e.g. the issues tab on the left-hand side.
More general tasks in the future are:
- better case (seen `base_test.scad`), waterproof design
- configuration of the LoRIS hardware via webfrontend
- usage of a real database, instead of sqlite
- better user/password management
- extend plot code for x/y axis labels
- Backend keepalive here / console
- sanity check/check if values allowed? (post request on node changes)
- Adapt measurement frequency on events
- and many more ...