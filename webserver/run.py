from flask import Flask, render_template, request
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash

import sqlite3
import json

app = Flask(__name__)
auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(username, password):
    users = json.load(open("webserver/web_configuration.json", "r"))[
        "users_web_credentials"
    ]
    if username in users and check_password_hash(users.get(username), password):
        return username


def getnodedata(id):
    con = sqlite3.connect("data.db")
    cur = con.cursor()
    cur.execute("PRAGMA journal_mode=wal;")

    # get time data
    cur.execute(
        "SELECT * FROM `node_data` WHERE node_id = ? AND last_updated >= datetime('now' , '-2 days') ORDER BY last_updated DESC",
        id,
    )
    data = []
    rows = cur.fetchall()

    # TODO REMOVE Demo Mode:
    if len(rows) == 0:
        cur.execute(
            "SELECT * FROM `node_data` WHERE node_id = ? ORDER BY last_updated DESC LIMIT 20;",
            id,
        )
        rows = cur.fetchall()

    for row in rows:
        # print(row[1])
        data.append((row[1], row[4]))

    return json.dumps(list(reversed(data)))


def getnodes():
    con = sqlite3.connect("data.db")

    cur = con.cursor()
    cur.execute("PRAGMA journal_mode=wal;")

    cur.execute(
        "SELECT *, MIN(`node_data`.water_level), MAX(`node_data`.water_level) FROM `nodes` LEFT JOIN `node_data` ON `nodes`.ID = `node_data`.node_id GROUP BY `nodes`.ID;"
    )

    data = []

    rows = cur.fetchall()
    for row in rows:
        # print(row)

        data.append(
            {
                "name": row[2],
                "id": row[0],
                "eui": row[1],
                "offset": row[3],
                "waters": row[4 + 2],
                "last_update": row[5 + 2],
                "min": row[6 + 5 + 2],
                "max": row[7 + 5 + 2],
                "lat": row[2 + 2],
                "lon": row[3 + 2],
                "color": 0,
            }
        )

    con.close()

    return data


def getnode(id):
    con = sqlite3.connect("data.db")

    cur = con.cursor()
    cur.execute("PRAGMA journal_mode=wal;")

    cur.execute(
        "SELECT *, MIN(`node_data`.water_level), MAX(`node_data`.water_level) FROM `nodes` LEFT JOIN `node_data` ON `nodes`.ID = `node_data`.node_id AND `nodes`.ID = ?;",
        (id,),
    )

    rows = cur.fetchall()

    con.close()
    if len(rows) == 0:
        return None
    return rows[0]


def update_node(id_post, eui, offset, name, lat, lon, waters):
    con = sqlite3.connect("data.db")
    cur = con.cursor()
    cur.execute("PRAGMA journal_mode=wal;")

    cur.execute(
        """UPDATE `nodes`
                SET offset = ?,
                    name = ?,
                    lat = ?,
                    lon = ?,
                    waters = ?
                WHERE id = ?;""",
        (offset, name, lat, lon, waters, id_post),
    )
    con.commit()

    con.close()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/map")
def map():
    return render_template("map.html")


@app.route("/nodes")
def nodes():
    return render_template("nodes.html", data=getnodes())


@app.route("/edit_node/<id>", methods=["GET", "POST"])
@auth.login_required
def edit_node(id):
    if request.method == "POST":
        id_post = request.form.get("id")
        eui = request.form.get("eui")
        name = request.form.get("name")

        offset = request.form.get("offset")
        lat = request.form.get("lat")
        lon = request.form.get("lon")
        waters = request.form.get("waters")
        # print(id_post, eui, offset, name, waters, lat, lon, waters)

        if id_post == id:
            # update here
            update_node(id, eui, offset, name, lat, lon, waters)

    return render_template("edit_node.html", data=getnode(id))


@app.route("/node/<id>")
def node(id):
    return render_template("node.html", data=getnode(id))


@app.route("/nodes_data")
def nodes_data():
    return json.dumps(getnodes())


@app.route("/node_data/<id>")
def node_data(id):
    return getnodedata(id)


if __name__ == "__main__":
    app.run(debug=False)
