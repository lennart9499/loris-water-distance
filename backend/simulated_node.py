from concurrent.futures import thread
from copyreg import constructor
import sqlite3
import time
from paho.mqtt import client as mqtt_client
import json
import random
import threading


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT Broker!")
    else:
        print("Failed to connect, return code %d\n", rc)


def publish(client, topic, replacement):
    topic = topic.replace("#", replacement)
    while True:
        msg = f'{{ "uplink_message": {{ "decoded_payload" : {{"Distance": {1400 + round(random.random() * 1000)}, "Supply_Voltage": 2932, "TX_Reason": "App_Cycle_Event"}}}}}}'
        result = client.publish(topic, msg)
        # print("Message sent!")
        time.sleep(60 * 15)  # 15 minuten


def main():
    config = json.load(open("backend/credentials.json", "r"))["mqtt_data_test"]

    client = mqtt_client.Client(f'python-mqtt-db-bridge-{config["broker"]}')

    client.username_pw_set(config["username"], config["password"])
    client.on_connect = on_connect
    client.connect(config["broker"], config["port"])
    client.loop_start()

    threads = []
    for x in ["eui-12341234/up", "eui-123123/up", "eui-1234512345/up"]:
        t = threading.Thread(target=publish, args=(client, config["topic"], x))
        t.start()
        threads.append(t)

    for t in threads:
        t.join()


if __name__ == "__main__":
    main()
