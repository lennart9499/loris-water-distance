from copyreg import constructor
import sqlite3
from time import sleep
from paho.mqtt import client as mqtt_client
import json


def on_message(client, userdata, msg):
    id = str(msg.topic.split("/")[-2])

    con = sqlite3.connect("data.db")
    cur = con.cursor()
    cur.execute("PRAGMA journal_mode=wal;")
    con.commit()

    data = json.loads(msg.payload.decode())
    print("------")
    # print(data)
    if "uplink_message" in data:
        print(data["uplink_message"]["decoded_payload"])
        # check if node id exists in database -> get id
        cur.execute("SELECT * FROM `nodes` WHERE EUI = ?", (id,))
        rows = cur.fetchall()

        # if not create it AND get id
        if len(rows) == 0:
            cur.execute(
                "INSERT INTO `nodes` (`name`, `eui`, `offset`, `lat`, `lon`, `waters`) VALUES('-', ?, '400', '0', '0', 'Bach')",
                (id,),
            )
            con.commit()

            cur.execute("SELECT * FROM `nodes` WHERE EUI = ?", (id,))
            rows = cur.fetchall()

        # insert in data table with id
        print(rows[0][3])  # complete height in cm
        cur.execute(
            "INSERT INTO `node_data` (`water_level`, `battery`, `node_id`) VALUES(?, ?, ?)",
            (
                round(
                    rows[0][3]
                    - data["uplink_message"]["decoded_payload"]["Distance"] / 10,
                    2,
                ),
                data["uplink_message"]["decoded_payload"]["Supply_Voltage"],
                rows[0][0],
            ),
        )
        con.commit()


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print(f"Connected to MQTT Broker!")
    else:
        print(f"Failed to connect, return code {rc}")


def main():
    config = json.load(open("backend/credentials.json", "r"))

    # normal mqtt server / ttn
    mqtt_data = config["mqtt_data"]

    client = mqtt_client.Client(f"python-mqtt-db-bridge")
    client.username_pw_set(mqtt_data["username"], mqtt_data["password"])
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(mqtt_data["broker"], mqtt_data["port"])
    client.subscribe(mqtt_data["topic"])
    client.loop_start()

    # normal mqtt server
    mqtt_data_test = config["mqtt_data_test"]

    if mqtt_data_test["active"]:
        client2 = mqtt_client.Client(f"python-mqtt-db-bridge-test")
        client2.username_pw_set(mqtt_data_test["username"], mqtt_data_test["password"])
        client2.on_connect = on_connect
        client2.on_message = on_message
        client2.connect(mqtt_data_test["broker"], mqtt_data_test["port"])
        client2.subscribe(mqtt_data_test["topic"])
        client2.loop_start()

    while True:
        sleep(60)


if __name__ == "__main__":
    main()
